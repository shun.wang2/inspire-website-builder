import { Plugin, Paragraph } from 'tiptap';

export default class Snippet extends Paragraph
{

    get name ()
    {
        return 'snippet'
    }

    get plugins ()
    {
        return [
            new Plugin( {
                props: {
                    handleDOMEvents: {
                        drop ( view, event )
                        {
                            event.preventDefault()
                            const coordinates = view.posAtCoords( { left: event.clientX, top: event.clientY } )
                            const content = event.dataTransfer.getData( 'text/plain' )
                            const transaction = view.state.tr.insertText( content, coordinates.pos )
                            view.dispatch( transaction )
                        }
                    }
                }
            } )
        ]
    }
}