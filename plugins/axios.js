export default function ( { $axios } )
{
    $axios.setHeader( 'Content-Type', 'application/json' )

    $axios.onRequest( config =>
    {
        console.log( config )
    } )
}