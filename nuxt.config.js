const webpack = require( 'webpack' )

let rootPATH = ""
let buildOptions = { extractCSS: true }
let server_root = '192.168.1.101'
if ( process.env.NODE_ENV === 'production' )
{
    buildOptions = {
        extractCSS: true,
        html: {
            minify: {
                collapseBooleanAttributes: true,
                decodeEntities: true,
                minifyCSS: false,
                minifyJS: true,
                processConditionalComments: true,
                removeEmptyAttributes: true,
                removeRedundantAttributes: true,
                trimCustomFragments: true,
                useShortDoctype: true
            }
        },
        optimization: {
            minimize: true,
            splitChunks: {
                cacheGroups: {
                    styles: {
                        name: 'styles',
                        test: /\.(css|vue)$/,
                        chunks: 'all',
                        enforce: true
                    }
                }
            }
        },
    }
}

export default {
    target: 'static',
    head: {
        title: process.env.npm_package_name || '',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: rootPATH + '/favicon.ico' }
        ]
    },
    css: [
        'video.js/dist/video-js.css',
        'vue2-dropzone/dist/vue2Dropzone.min.css'
    ],
    plugins: [
        { src: '~plugins/vue2-dropzone', ssr: false },
        { src: '~plugins/vue-blob-json-csv', ssr: false },
        { src: '~plugins/vuex', ssr: false },
        { src: '~plugins/vue-video-player.js', ssr: false },
        { src: '~/plugins/axios', ssr: false },
    ],
    components: true,
    modules: [
        'bootstrap-vue/nuxt',
        '@nuxtjs/proxy',
        [ '@nuxtjs/axios', {
            proxy: true,
        } ],
        '@nuxtjs/dotenv',
    ],
    proxy: {
        '/template': { target: `http://${ server_root }/ajax/template/`, changeOrigin: true, pathRewrite: { '^/template': '' } },
        '/product': { target: `http://${ server_root }/ajax/product/`, changeOrigin: true, pathRewrite: { '^/product': '' } },
        '/base64': { target: `http://${ server_root }/ajax/cms/baseImgUpload`, changeOrigin: true },
    },
    server: {
        port: 5000,
        host: '0.0.0.0'
    },
    buildModules: [ "@nuxtjs/svg" ],
    build: {
        ...buildOptions,
        plugins: [
            new webpack.ProvidePlugin( {
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
            } )
        ],
    },
    generate: {
        subFolders: false,
    },
    router: {
        base: rootPATH,
        fallback: false,
        extendRoutes ( routes, resolve )
        {
            let index = {
                name: 'home',
                path: '/',
                component: resolve( __dirname, 'pages/home/edit.vue' )
            }

            routes.push( index )
        }
    }
}
