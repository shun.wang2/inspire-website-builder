export const state = () => ( {
    active: 1,
    temp_img: {
        org: require( '@/assets/png/default_img.png' ),
        res: require( '@/assets/png/default_img.png' ),
        width: '',
        height: '',
    }
} )

export const getters = {
    active: state => state.active,
    temp_img: state => state.temp_img,
}

export const actions = {
    async base64ToImg ( { state, dispatch, commit }, payload )
    {
        let data = new FormData();
        data.append( 'base_img', payload );
        return await this.$axios.$post( '/base64', data ).then( res =>
        {
            return res
        } )
    }
}

export const mutations = {
    [ 'UPDATE_ACTIVE' ] ( state, payload )
    {
        state.active = payload
    },
    [ 'UPDATE_TEMP_IMAGE' ] ( state, { key, val } )
    {
        state.temp_img[ key ] = val
    }
}