export const mutations = {
    [ 'UPDATE_SECONDARYCOMPONENT_CARD_VALUE' ] ( state, { key, second_key, thrid_key, val } )
    {
        let index = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( key );
        state.secondaryComponent[ index ].content[ 'cards' ][ thrid_key ][ second_key ] = val
    },
    [ 'DELETE_SECONDARYCOMPONENT_CARD_VALUE' ] ( state, { key, product_index } )
    {
        let index = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( key );
        state.secondaryComponent[ index ].content[ 'cards' ].splice( product_index, 1 )
    },
    [ 'PUSH_SECONDARYCOMPONENT_CARD_VALUE' ] ( state, { key, product } )
    {
        let index = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( key );
        state.secondaryComponent[ index ].content[ 'cards' ].push( product )
    },
}