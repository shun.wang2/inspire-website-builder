let text_content_default = ( heading = 1 ) =>
{
    return {
        "type": "doc",
        "content": [
            {
                "type": "heading",
                "attrs": {
                    "level": heading
                },
                "content": [
                    {
                        "type": "text",
                        "marks": [
                            {
                                "type": "bold"
                            }
                        ],
                        "text": "測試標題"
                    }
                ]
            },
            {
                "type": "paragraph",
                "content": [
                    {
                        "type": "text",
                        "text": "這個內容是測試使用，請點擊編輯後，修改為您的內容。"
                    }
                ]
            }
        ]
    }
}

export const state = () => ( {
    primaryComponent: [
        {
            name: "texts",
            id: "A",
            link: false,
            content: {
                data: text_content_default()
            }
        },
        {
            name: "images",
            id: "B",
            link: false,
            content: {
                img: 'http://192.168.43.113/public/default/img/default_img.png',
                url: '',
                alt: '',
                title: ''
            }
        },
        {
            name: "videos",
            id: "C",
            link: true,
            content: {
                url: '<iframe width="560" height="315" src="https://www.youtube.com/embed/KMX1mFEmM3E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
            }
        },
        {
            name: "codes",
            id: "D",
            link: false,
            content: {
                script: ''
            }
        },
        {
            name: "products",
            id: "E",
            link: false,
            content: {
                products: [
                    {
                        product_image: 'http://192.168.43.113/public/default/img/product_img_default.png',
                        product_name: '護脊書包',
                        product_brief: "全球新一代智能秒開磁扣護脊書包-一年級二年級護脊書包▶TigerFamily 小學者超輕量護脊書包-恬靜小馬",
                        price_list: '320',
                        price_selling: '450',
                        product_id: "E0",
                        product_spec_id: "E0",
                        product_spec_name: ""
                    },
                    {
                        product_image: 'http://192.168.43.113/public/default/img/product_img_default.png',
                        product_name: '護脊書包',
                        product_brief: "全球新一代智能秒開磁扣護脊書包-一年級二年級護脊書包▶TigerFamily 小學者超輕量護脊書包-恬靜小馬",
                        price_list: '250',
                        price_selling: '360',
                        product_id: "E1",
                        product_spec_id: "E0",
                        product_spec_name: ""
                    },
                    {
                        product_image: 'http://192.168.43.113/public/default/img/product_img_default.png',
                        product_name: '護脊書包',
                        product_brief: "全球新一代智能秒開磁扣護脊書包-一年級二年級護脊書包▶TigerFamily 小學者超輕量護脊書包-恬靜小馬",
                        price_list: '300',
                        price_selling: '400',
                        product_id: "E2",
                        product_spec_id: "E0",
                        product_spec_name: ""
                    },
                ]
            }
        },
    ],
    secondaryComponent: [],
} )

export const getters = {
    primaryComponent: state => state.primaryComponent,
    secondaryComponent: state => state.secondaryComponent,
}

export const actions = {
    async getTemplate ( { state, commit, dispatch }, id )
    {
        console.log( id );
        return await this.$axios.$get( '/template/get_template', {
            params: { shop_id: id }
        } ).then( res =>
        {
            if ( Object.values( res ).length > 0 )
            {
                let a = []
                let children = Object.values( Object.values( res )[ 0 ].children )

                children.forEach( ( item, index ) =>
                {
                    a.push( {
                        name: item.unit_type,
                        id: item.id,
                        link: item.data.is_link === "0" ? false : true,
                        content: item.data.content
                    } )
                } )
                commit( 'UPDATE_SECONDARYCOMPONENT', a )
            }
        } )
    },
    async addTemplate ( { state, commit, dispatch }, id )
    {
        let data = new FormData();
        data.append( 'shop_id', id );
        data.append( 'json', JSON.stringify( state.secondaryComponent ) );
        return await this.$axios.$post( '/template/add', data ).then( res =>
        {
            console.log( 'res', res )
        } )
    },
    async getProduct ( { state, dispatch, commit }, { id, index } )
    {
        return await this.$axios.$get( '/product/get_product', {
            params: { shop_id: id }
        } ).then( res =>
        {
            commit( 'UPDATE_SECONDARYCOMPONENT_VALUE', {
                key: index,
                second_key: "products",
                val: res
            } )
        } )
    }
}

export const mutations = {
    [ 'UPDATE_PRIMARYCOMPONENT' ] ( state, payload )
    {
        state.primaryComponent = JSON.parse( JSON.stringify( payload ) );
    },
    [ 'UPDATE_SECONDARYCOMPONENT' ] ( state, payload )
    {
        state.secondaryComponent = JSON.parse( JSON.stringify( payload ) );
    },
    [ 'DUPLICATE_SECONDARYCOMPONENT' ] ( state, payload )
    {
        let duplicateIndex = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( payload[ 0 ].id );
        state.secondaryComponent.splice( duplicateIndex + 1, 0, {
            id: payload[ 0 ].id + duplicateIndex.toString() + payload[ 1 ],
            link: payload[ 0 ].link,
            name: payload[ 0 ].name,
            content: payload[ 0 ].content,
        } );

        state.secondaryComponent = JSON.parse( JSON.stringify( state.secondaryComponent ) );
    },
    [ 'REMOVE_SECONDARYCOMPONENT' ] ( state, payload )
    {
        let index = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( payload );
        state.secondaryComponent.splice( index, 1 )

        state.secondaryComponent = JSON.parse( JSON.stringify( state.secondaryComponent ) );
    },
    [ 'UPDATE_SECONDARYCOMPONENT_VALUE' ] ( state, { key, second_key, val } )
    {
        let index = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( key );

        state.secondaryComponent[ index ].content[ second_key ] = val
    },
    [ 'UPDATE_SECONDARYCOMPONENT_PRODUCT_VALUE' ] ( state, { key, second_key, thrid_key, val } )
    {
        let index = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( key );
        state.secondaryComponent[ index ].content[ 'products' ][ thrid_key ][ second_key ] = val
    },
    [ 'DELETE_SECONDARYCOMPONENT_PRODUCT_VALUE' ] ( state, { key, product_index } )
    {
        let index = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( key );
        state.secondaryComponent[ index ].content[ 'products' ].splice( product_index, 1 )
    },
    [ 'PUSH_SECONDARYCOMPONENT_PRODUCT_VALUE' ] ( state, { key, product } )
    {
        let index = state.secondaryComponent
            .map( ( i ) => i.id )
            .indexOf( key );
        state.secondaryComponent[ index ].content[ 'products' ].push( product )
    },
}